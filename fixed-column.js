﻿(function ($) {
    var defaultOptions = {
        'z-index': 1,
        'background-color': 'white'
    };
    
    var columns = [];

    function isTableVisibleButColumnNot(elem, overflowEl) {
        var docViewTop = $(window).scrollTop();
        var docViewBottom = docViewTop + $(window).height();

        var elemTop = $(elem).offset().top;
        var elemBottom = elemTop + $(elem).height();

        var isOverflowScrolled = overflowEl.scrollLeft() > 0;

        var tableIsVisible = ((elemTop <= docViewBottom) && (elemTop >= docViewTop))
            || ((elemBottom <= docViewBottom) && (elemBottom >= docViewTop))
            || ((elemTop <= docViewTop) && (elemBottom >= docViewBottom));

        return tableIsVisible && isOverflowScrolled;
    };

    function getRelativeTopOffset(relative) {
        var docViewTop = $(window).scrollTop();
        var relativeTop = $(relative).offset().top;

        return docViewTop - relativeTop;
    }

    function checkTableColumn() {
        for (var i = 0; i < columns.length; i++) {
            var prevLeft = columns[i].prevLeft;
            if (prevLeft != columns[i].overflowEl.scrollLeft()) {
                columns[i].prevLeft = columns[i].overflowEl.scrollLeft();
                
                columns[i].cloned.hide();
                if (isTableVisibleButColumnNot(columns[i].source, columns[i].overflowEl)) {
                    var widthOfColumn = getWidthOfColumn(columns[i].source, columns[i].cloned);
                    columns[i].cloned
                        .css('left', columns[i].source.position().left + 2 * columns[i].overflowEl.scrollLeft())
                        .css('height', columns[i].source.height())
                        .css('top', columns[i].source.position().top + columns[i].overflowEl.scrollTop())
                        .css('width', widthOfColumn)
                        .show();
                    if (columns[i].heightForEmptyTdIsSet != true) {
                        setHeightOfEmptyTd(columns[i].source, columns[i].cloned);
                        columns[i].heightForEmptyTdIsSet = true;
                    }
                }
            }
        }
    }
    
    function setHeightOfEmptyTd(source, target) {
        source.find('td[data-title][data-empty]').each(function (index) {
            target.find('td[data-title][data-empty]:eq(' + index + ')').height($(this).outerHeight());
        });
    }

    function clearCache() {
        for (var i = 0; i < columns.length; i++) {
            if (columns[i].source.closest('body').length == 0) {
                columns.splice(i, 1);
                i--;
            }
        }
    }

    function getWidthOfColumn(source) {
        var width = 0;
        source.find('tbody > tr:eq(0) > td[data-title]').each(function () {
            width += $(this).width();
        });
        return width;
    }
    
    function removeExcessTDs(table) {
        table.find('tr').each(function() {
            $(this).find('td:gt(0)').remove();
        });
    }

    $.fn.fixedColumn = function (options) {
        defaultOptions = $.extend({}, defaultOptions, options);

        clearCache();
        $(window).on('scroll', function() {
            checkTableColumn();
        });

        $(window).on('resize', function() {
            for (var i = 0; i < columns.length; i++) {
                columns[i].heightForEmptyTdIsSet = false;
            }
            checkTableColumn();
        });

        return this.each(function () {
            if ($(this).is('table') && $(this).find('thead').length > 0) {
                var parent = $(this).parent();
                var relative = $(this).closest('[style*="relative"]');
                if (relative.length == 0) {
                    relative = $('html');
                }
                var overflowEl = $(this).closest('[style*="overflow"]');
                if (overflowEl.length !=== 0) {
                    overflowEl.on('scroll', function () {
                        checkTableColumn();
                    });
                }
                var cloned = $(this).clone();
                cloned.find('td:not([data-title])').remove();
                cloned.removeAttr('id');
                cloned.hide()
                    .appendTo(parent)
                    .css('background-color', defaultOptions['background-color'])
                    .css('position', 'absolute')
                    .css('z-index', defaultOptions['z-index']);

                columns.push({ source: $(this), cloned: cloned, prevLeft:0, overflowEl: overflowEl, relative: relative });
            }
        });
    };

})(jQuery);